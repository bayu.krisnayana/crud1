<!DOCTYPE html>
<html>
<head>
    <title>Admin</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
        integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/fd8370ec87.js" crossorigin="anonymous"></script>
</head>
<body>
    <div id="navbar" class="mb-4">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand" href="#">Data Mahasiswa</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a href="/" class="nav-link"> <i
                                class="fas fa-sign-out-alt"></i> Logout</a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>

	<div class="row mb-4 pl-4">
        <div class="pull-left ">
            <h4>Daftar Mahasiswa</h4>
        </div>
        <div class="pull-right pl-3 mb-3">
            <a href="/tambah">
                <button class="btn btn-primary">Tambah Data Mahasiswa</button>
            </a>
        </div>

        <table class="table ">
            <thead>
                <tr>
                    <th>
                        Id
                    </th>
                    <th>Nama</th>
                    <th>Nim</th>
                    <th>Kelas</th>
                    <th>Prodi</th>
                    <th>Fakultas</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($mahasiswa as $p)
                <tr>
                    <td>{{ $p->id }}</td>
                    <td>{{ $p->nama_mahasiswa }}</td>
			        <td>{{ $p->nim_mahasiswa }}</td>
			        <td>{{ $p->kelas_mahasiswa }}</td>
			        <td>{{ $p->prodi_mahasiswa }}</td>
                    <td>{{ $p->fakultas_mahasiswa }}</td>
			        <td>
				        <a href="/edit/{{ $p->id }}">Edit</a>
				        |
				        <a href="/hapus/{{ $p->id }}">Hapus</a>
			        </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

</body>
</html>